package model.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Marcus on 21.11.2017.
 */
public class ProvenanceNode {

    private String name;
    private ProvenanceElementType provenanceElementType;
    private String id;

    private List<ProvenanceRelationship> provenanceRelationships;
    private List<String> targets;
    private double positionX;
    private double positionY;
    private HashMap<String, ArrayList<String>> descriptionMap;
    private String description;

    public ProvenanceNode(String id, String name, ProvenanceElementType provenanceElementType, double positionX, double positionY) {
        this.id = id;
        this.provenanceElementType = provenanceElementType;
        this.targets = new ArrayList<>();
        this.positionX = positionX;
        this.positionY = positionY;
        this.description="";


        this.name = name;


        this.provenanceRelationships = new LinkedList<>();
        this.descriptionMap = new HashMap<>();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HashMap<String, ArrayList<String>> getDescriptionMap() {
        return descriptionMap;
    }

    public double getPositionX() {
        return positionX;
    }

    public void setPositionX(double positionX) {
        this.positionX = positionX;
    }

    public double getPositionY() {
        return positionY;
    }

    public void setPositionY(double positionY) {
        this.positionY = positionY;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ProvenanceNode{" +
                "name='" + name + '\'' +
                ", provenanceElementType=" + provenanceElementType +
                ", id='" + id + '\'' +
                ", provenanceRelationships=" + provenanceRelationships +
                ", targets=" + targets +
                ", positionX=" + positionX +
                ", positionY=" + positionY +
                '}';
    }

    public List<ProvenanceRelationship> getProvenanceRelationships() {
        return provenanceRelationships;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProvenanceElementType getProvenanceElementType() {
        return provenanceElementType;
    }

    public void setProvenanceElementType(ProvenanceElementType provenanceElementType) {
        this.provenanceElementType = provenanceElementType;
    }

}
