package model.graph;

/**
 * Created by Marcus on 29.10.2017.
 */

public enum ProvenanceElementType {
    Artifact, Process, Agent
}
