package model.graph;

/**
 * Created by Marcus on 21.11.2017.
 */


public enum RelationshipType implements org.neo4j.graphdb.RelationshipType {
    USED, WAS_GENERATED_BY, WAS_CONTROLLED_BY, WAS_TRIGGERED_BY, WAS_DERIVED_FROM, NONE;
}
