package model.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 21.11.2017.
 */
public class ProvenanceGraph {


    private List<ProvenanceNode> vertices;

    public ProvenanceGraph() {

        this.vertices = new ArrayList<>();
    }


    public List<ProvenanceNode> getVertices() {
        return vertices;
    }
}
