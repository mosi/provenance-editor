package model.graph;

public class ProvenanceRelationship {

    private ProvenanceNode sourceProvenanceNode;
    private ProvenanceNode targetProvenanceNode;
    private String id;
    private RelationshipType relationshipType;

    public ProvenanceRelationship(ProvenanceNode sourceProvenanceNode, ProvenanceNode targetProvenanceNode, String id, RelationshipType relationshipType) {
        this.sourceProvenanceNode = sourceProvenanceNode;
        this.targetProvenanceNode = targetProvenanceNode;
        this.id = id;
        this.relationshipType = relationshipType;

    }

    @Override
    public String toString() {
        return "ProvenanceRelationship{" +
                "sourceProvenanceNode=" + sourceProvenanceNode +
                ", targetProvenanceNode=" + targetProvenanceNode +
                ", id='" + id + '\'' +
                ", relationshipType=" + relationshipType +
                '}';
    }

    public ProvenanceNode getSourceProvenanceNode() {
        return sourceProvenanceNode;
    }

    public void setSourceProvenanceNode(ProvenanceNode sourceProvenanceNode) {
        this.sourceProvenanceNode = sourceProvenanceNode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RelationshipType getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(RelationshipType relationshipType) {
        this.relationshipType = relationshipType;
    }

    public ProvenanceNode getTargetProvenanceNode() {
        return targetProvenanceNode;
    }

    public void setTargetProvenanceNode(ProvenanceNode targetProvenanceNode) {
        this.targetProvenanceNode = targetProvenanceNode;
    }
}
