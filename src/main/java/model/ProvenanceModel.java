package model;


import com.google.common.io.Files;
import model.graph.ProvenanceGraph;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 26.11.2017.
 */
public class ProvenanceModel {
    private List<ProvenanceGraph> provenanceGraphs;
    private List<File> files;
    private GraphDatabaseFactory provenanceFactory;
    private GraphDatabaseService provenanceDatabase;


    public ProvenanceModel() {
        provenanceFactory = new GraphDatabaseFactory();
        File temporaryDatabaseDirectory = Files.createTempDir();
        provenanceDatabase = provenanceFactory.newEmbeddedDatabase(temporaryDatabaseDirectory);
        clearDatabase();
        this.provenanceGraphs = new ArrayList<>();
        this.files = new ArrayList<>();
    }

    private void clearDatabase() {
        provenanceDatabase.execute("Match (n) Detach Delete n");
    }

    public GraphDatabaseService getProvenanceDatabase() {
        return provenanceDatabase;
    }

    public List<File> getFiles() {
        return files;
    }

    public List<ProvenanceGraph> getProvenanceGraphs() {
        return provenanceGraphs;
    }
}
