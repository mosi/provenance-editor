import controller.editor.ProvenanceEditorController;
import model.ProvenanceModel;

import javax.swing.*;


public class MainApplication {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            ProvenanceModel provenanceModel = new ProvenanceModel();
            ProvenanceEditorController controller = new ProvenanceEditorController(provenanceModel);
            controller.showView();
        } catch (ClassNotFoundException | InstantiationException | UnsupportedLookAndFeelException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
