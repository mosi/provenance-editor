package controller.editor.subcontroller;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import controller.MainController;
import controller.editor.ProvenanceEditorController;
import model.ProvenanceModel;
import model.graph.*;
import model.graph.RelationshipType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.neo4j.graphdb.*;
import view.MainView;

import javax.swing.*;
import java.util.*;

/**
 * Created by Marcus on 21.02.2018.
 */

public class DatabaseController extends MainController{
    private ProvenanceModel provenanceModel;
    private ProvenanceEditorController controller;

    public DatabaseController(ProvenanceModel provenanceModel, ProvenanceEditorController controller) {
        this.provenanceModel = provenanceModel;
        this.controller = controller;
    }


    public void createNewNodeInDatabase(String nodeId, ProvenanceElementType provenanceElementType, String nodeName, ProvenanceNode provenanceNode) {
        GraphDatabaseService provenanceDatabase = provenanceModel.getProvenanceDatabase();
        try (Transaction tx = provenanceDatabase.beginTx()) {
            addNodeToDatabase(nodeId, provenanceElementType, nodeName, provenanceDatabase, provenanceNode);
            tx.success();
            tx.close();
            //debugShowAllNodesInDatabase(provenanceDatabase);
        } catch (QueryExecutionException e) {
            e.printStackTrace();
        }
    }

    private void addNodeToDatabase(String nodeId, ProvenanceElementType provenanceElementType, String nodeName, GraphDatabaseService provenanceDatabase, ProvenanceNode provenanceNode) {
        Node databaseNode = provenanceDatabase.createNode(Label.label("Node"));
        databaseNode.setProperty("type", provenanceElementType.toString());
        databaseNode.setProperty("id", nodeId);
        databaseNode.setProperty("value", nodeName);
        JSONParser parser = new JSONParser();
        try {
            JSONObject JSONObject = (JSONObject) parser.parse("{" + provenanceNode.getDescription() + " }");
            for (Object description : JSONObject.entrySet()) {
                Map.Entry pair = (Map.Entry) description;
                String key = pair.getKey().toString();
                String value = pair.getValue().toString();
                ArrayList<String> descriptionList = new ArrayList<String>();
                String[] values = value.split(",");
                descriptionList.addAll(Arrays.asList(values));
                provenanceNode.getDescriptionMap().put(key, descriptionList);
                //System.out.println("Key : " + key + " Value " + descriptionList.toString());

                addDescriptionToDatabase(provenanceNode.getId(), key, descriptionList);
            }
        } catch (ParseException parseException) {
            String title = "Incorrect JSON Syntax";
            MainView mainView = controller.getMainView();
            JOptionPane.showMessageDialog(mainView, parseException.getMessage(), title, JOptionPane.ERROR_MESSAGE);
            parseException.printStackTrace();
        }

    }

    private void debugShowAllNodesInDatabase(GraphDatabaseService provenanceDatabase) {
        Result result = provenanceDatabase.execute("Match (c:Node) where c.type = \"Agent\" or c.type=\"Artifact\" or c.type=\"Process\" Return c.type, c.id, c.value");
        while (result.hasNext()) {
            Map<String, Object> row = result.next();
            for (String key : result.columns()) {
                System.out.printf("%s = %s%n", key, row.get(key));
            }
        }

    }
    public void addDescriptionToDatabase(String id, String key, ArrayList<String> descriptionList) {
        GraphDatabaseService databaseService = provenanceModel.getProvenanceDatabase();
        try (Transaction tx = databaseService.beginTx()) {
            Node node = databaseService.findNode(Label.label("Node"), "id", id);
            node.setProperty(key, descriptionList.toString());
            tx.success();
            tx.close();
        }catch (MultipleFoundException e){
            e.printStackTrace();
        }
    }

    public void handleExecuteQueryButton(JTextArea cypherTextArea) {
        resetHighlighting();
        try {
            GraphDatabaseService provenanceDatabase = provenanceModel.getProvenanceDatabase();
            try (Transaction tx = provenanceDatabase.beginTx()) {
                Result result = provenanceDatabase.execute(cypherTextArea.getText());

                printQueryResultIntoLog(result);
                tx.success();
                tx.close();

            }
        } catch (QueryExecutionException e) {
            String title = "Problem with Query";
            MainView mainView = controller.getMainView();
            JOptionPane.showMessageDialog(mainView, e.getMessage(), title, JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    private void printQueryResultIntoLog(Result result) {
        JTextArea resultLogArea = controller.getMainView().getQueryView().getQueryResultLogView().getResultLogArea();
        resultLogArea.setText("");
        if (!result.hasNext()) {
            resultLogArea.setText("Empty Resultset");
        }
        while (result.hasNext()) {
            Map<String, Object> row = result.next();
            for (String key : result.columns()) {
                String areaResult = (key + " = " + row.get(key) + "  ");
                resultLogArea.setText(resultLogArea.getText() + areaResult);
                highlightQueryResults(row, key);
            }
            resultLogArea.setText(resultLogArea.getText() + "\n");
        }
    }

    private void highlightQueryResults(Map<String, Object> row, String key) {


        String[] split = key.split("\\.");
        if (split.length<=1){

        }else{
            String attribute = split[1];
            if(attribute.equals("id") || attribute.equals("relationId")){
                mxGraph graph = controller.getGraph();
                graph.selectAll();
                Object[] selectionCells = graph.getSelectionCells();
                for (Object cells : selectionCells){
                    if (((mxCell) cells).getId().equals(row.get(key))){
                        mxIGraphModel model = graph.getModel();
                        model.beginUpdate();
                        if (((mxCell) cells).isVertex()){
                            graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, "orange", new Object[]{cells});
                        }else{
                            graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, "orange", new Object[]{cells});
                        }

                        graph.getSelectionModel().clear();
                        model.endUpdate();
                    }
                }
            }
        }

    }

    private void resetHighlighting(){
        mxGraph graph = controller.getGraph();
        graph.selectAll();
        Object[] selectionCells = graph.getSelectionCells();
        for (Object cells : selectionCells){
            mxIGraphModel model = graph.getModel();
            model.beginUpdate();
            graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, "#C3D9FF", new Object[]{cells});
            graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, "#6482B9", new Object[]{cells});
            graph.getSelectionModel().clear();
            model.endUpdate();
        }
    }

    public void createNewRelationshipInDatabase(ProvenanceNode sourceNode, ProvenanceNode targetNode, RelationshipType relationshipType, GraphDatabaseService provenanceDatabase) {
        try (Transaction tx = provenanceDatabase.beginTx()) {
            Node sourceDatabaseNode = provenanceDatabase.findNode(Label.label("Node"), "id", sourceNode.getId());
            Node targetDatabaseNode = provenanceDatabase.findNode(Label.label("Node"), "id", targetNode.getId());
            Relationship relationshipTo = sourceDatabaseNode.createRelationshipTo(targetDatabaseNode, relationshipType);

            ProvenanceRelationship provenanceRelationshipFromSourceAndTarget = controller.getProvenanceRelationshipFromSourceAndTarget(sourceNode, targetNode);
            relationshipTo.setProperty("relationId", provenanceRelationshipFromSourceAndTarget.getId());
            tx.success();
            tx.close();
           /* Result result = provenanceDatabase.execute("Match (n)-[r]-(m) return n.value, type(r), m.value");
            while (result.hasNext()) {
                System.out.println("*********");
                System.out.println(result.next());
            }
            */
        }catch (MultipleFoundException e){
           e.printStackTrace();
        }
    }

    public void deleteElementsFromDatabase(ProvenanceGraph provenanceGraph, mxCell cell) {
        GraphDatabaseService provenanceDatabase = provenanceModel.getProvenanceDatabase();
        try (Transaction tx = provenanceDatabase.beginTx()) {
            if (cell.isVertex()) {
                deleteNodeFromDatabase(provenanceGraph, cell, provenanceDatabase, tx);
            } else if (cell.isEdge()) {
                deleteRelationshipFromDatabase(provenanceGraph, cell, provenanceDatabase, tx);
            }
            tx.success();
            tx.close();
            //printDatabaseDebugMessage(provenanceDatabase);
        } catch (QueryExecutionException e) {
            e.printStackTrace();
        }
    }

    private void deleteRelationshipFromDatabase(ProvenanceGraph provenanceGraph, mxCell cell, GraphDatabaseService provenanceDatabase, Transaction tx) {
        ProvenanceRelationship provenanceRelationshipById = controller.getProvenanceRelationshipById(provenanceGraph, cell
                .getId());
        ProvenanceNode sourceProvenanceNode = provenanceRelationshipById.getSourceProvenanceNode();
        String nodeId = sourceProvenanceNode.getId();
        RelationshipType relationshipType = provenanceRelationshipById.getRelationshipType();
        provenanceDatabase.execute("Match (n {id:'" + nodeId + "'})-[r:" +
                relationshipType + "]-() DELETE r");
    }

    private void deleteNodeFromDatabase(ProvenanceGraph provenanceGraph, mxCell cell, GraphDatabaseService provenanceDatabase, Transaction tx) {
        ProvenanceNode provenanceNodeById = controller.getProvenanceNodeById(provenanceGraph, cell.getId());
        String id = provenanceNodeById.getId();
        provenanceDatabase.execute("Match (n {id: '" + id + "'}) Detach delete n");
    }

    private void printDatabaseDebugMessage(GraphDatabaseService provenanceDatabase) {
        Result result = provenanceDatabase.execute("Match (c:Node) where c.type = \"Agent\" or " +
                "c.type=\"Artifact\" or c.type=\"Process\" Return c.type, c.id, c.value");
        System.out.println("************************");
        while (result.hasNext()) {
            Map<String, Object> row = result.next();
            for (String key : result.columns()) {
                System.out.printf("%s = %s%n", key, row.get(key));
            }
        }
    }


}
