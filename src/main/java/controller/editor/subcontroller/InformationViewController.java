package controller.editor.subcontroller;

import controller.editor.ProvenanceEditorController;
import model.ProvenanceModel;
import model.graph.ProvenanceNode;
import model.graph.ProvenanceRelationship;
import model.graph.RelationshipType;
import view.information.InformationView;
import view.information.graphelements.NodeView;
import view.information.graphelements.RelationshipView;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Marcus on 21.02.2018.
 */
//FIXME: why dont you use here sub-classing?

public class InformationViewController {
    private ProvenanceEditorController controller;
    private ProvenanceModel provenanceModel;

    public InformationViewController(ProvenanceModel provenanceModel, ProvenanceEditorController controller) {
        this.controller = controller;
        this.provenanceModel = provenanceModel;
    }


    public void updateNodeInformationView(InformationView informationView, ProvenanceNode provenanceNode) {
        clearInformationView(informationView);
        NodeView nodeView = informationView.getNodeView();
        clearNodeView(nodeView);
        fillNodeViewInformation(nodeView, provenanceNode);
        informationView.add(nodeView, BorderLayout.WEST);
        nodeView.setVisible(true);

    }

    public void updateEdgeInformationView(InformationView informationView, ProvenanceRelationship relationship) {
        clearInformationView(informationView);
        RelationshipView relationshipView = informationView.getRelationshipView();
        clearEdgeView(relationshipView);
        fillEdgeViewInformation(relationshipView, relationship);
        informationView.add(relationshipView, BorderLayout.WEST);
        relationshipView.setVisible(true);


    }

    private void clearInformationView(InformationView informationView) {
        NodeView nodeView = informationView.getNodeView();
        RelationshipView relationshipView = informationView.getRelationshipView();
        nodeView.setVisible(false);
        relationshipView.setVisible(false);
    }

    private void fillEdgeViewInformation(RelationshipView relationshipView, ProvenanceRelationship relationship) {
        JTextField targetTextField = relationshipView.getTargetTextField();
        ProvenanceNode targetProvenanceNode = relationship.getTargetProvenanceNode();
        String id = targetProvenanceNode.getId();
        targetTextField.setText(id);
        relationshipView.getSourceTextField().setText(relationship.getSourceProvenanceNode().getName());
        relationshipView.getIdTextfield().setText(relationship.getId());
        relationshipView.getDependencyTextField().setText(relationship.getRelationshipType().name());
        if (relationship.getRelationshipType() == RelationshipType.WAS_TRIGGERED_BY) {
            relationshipView.getRoleTextField().setEditable(false);
        }
        if (relationship.getRelationshipType() == RelationshipType.WAS_DERIVED_FROM) {
            relationshipView.getRoleTextField().setEditable(false);
        }
    }

    private void fillNodeViewInformation(NodeView nodeView, ProvenanceNode provenanceNode) {
        String provenanceElementType = provenanceNode.getProvenanceElementType().toString();
        nodeView.getTypeTextField().setText(provenanceElementType);
        String id = provenanceNode.getId();
        nodeView.getIdTextfield().setText(id);
        nodeView.getDescriptionTextArea().setText(provenanceNode.getDescription());
        for (ProvenanceRelationship relationship : provenanceNode.getProvenanceRelationships()) {
            ProvenanceNode target = relationship.getTargetProvenanceNode();
            JTextField relationshipTextField = nodeView.getRelationshipTextField();
            String text = relationshipTextField.getText();
            relationshipTextField.setText(text + target.getName() + "  ");
        }

    }

    public  void clearNodeView(NodeView nodeView) {

        nodeView.getIdTextfield().setText("");
        nodeView.getRelationshipTextField().setText("");
        nodeView.getTypeTextField().setText("");
        nodeView.getDescriptionTextArea().setText("");
    }

    public void clearEdgeView(RelationshipView relationshipView) {
        relationshipView.getIdTextfield().setText("");
        relationshipView.getDependencyTextField().setText("");
        relationshipView.getSourceTextField().setText("");
        relationshipView.getTargetTextField().setText("");
    }

}
