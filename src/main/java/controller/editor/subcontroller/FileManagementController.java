package controller.editor.subcontroller;

import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;
import controller.editor.ProvenanceEditorController;
import model.ProvenanceModel;
import model.graph.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.neo4j.graphdb.GraphDatabaseService;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Marcus on 21.02.2018.
 */
//FIXME: why dont you use here sub-classing?

public class FileManagementController {
    private ProvenanceEditorController controller;
    private ProvenanceModel provenanceModel;

    public FileManagementController(ProvenanceModel provenanceModel, ProvenanceEditorController controller) {
        this.provenanceModel = provenanceModel;
        this.controller = controller;
    }

    public void saveGraph(ProvenanceGraph provenanceGraph, String filePath) throws IOException {
        File provenanceFile = new File(filePath);
        FileWriter fileWriter = new FileWriter(provenanceFile, true);
        JSONObject graphObject = createJsonObject(provenanceGraph);
        graphObject.writeJSONString(fileWriter);
        fileWriter.flush();
    }

    @SuppressWarnings("unchecked")
    private JSONObject createJsonObject(ProvenanceGraph provenanceGraph) {
        JSONObject graphObject = new JSONObject();
        graphObject.put("name", "graph");
        JSONArray nodes = new JSONArray();
        for (ProvenanceNode node : provenanceGraph.getVertices()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", node.getName());
            jsonObject.put("type", node.getProvenanceElementType().toString());
            jsonObject.put("id", node.getId());
            jsonObject.put("coordinateX", node.getPositionX());
            jsonObject.put("coordinateY", node.getPositionY());
            jsonObject.put("description", node.getDescription());
            JSONArray list = new JSONArray();
            for (ProvenanceRelationship provenanceRelationship : node.getProvenanceRelationships()) {
                JSONObject relationship = new JSONObject();
                relationship.put("source", provenanceRelationship.getSourceProvenanceNode().getId());
                relationship.put("target", provenanceRelationship.getTargetProvenanceNode().getId());
                relationship.put("id", provenanceRelationship.getId());
                relationship.put("type", provenanceRelationship.getRelationshipType().toString());
                list.add(relationship);
            }
            jsonObject.put("EdgeTarget", list);

            nodes.add(jsonObject);

        }

        graphObject.put("Nodes", nodes);
        return graphObject;
    }

    public void loadGraph(String filePath) throws IOException, ParseException {
        controller.createNewProvenanceModel();
        controller.setCurrentFileName(filePath);
        parseFileToProvenanceModel(filePath);
        ProvenanceGraph provenanceGraph = provenanceModel.getProvenanceGraphs().get(0);
        createGraphFromModel(provenanceGraph);

    }

    private void createGraphFromModel(ProvenanceGraph provenanceGraph) {
        createNodeFromModel(provenanceGraph);
        createRelationshipsFromModel(provenanceGraph);
    }

    private void createNodeFromModel(ProvenanceGraph provenanceGraph) {
        for (ProvenanceNode node : provenanceGraph.getVertices()) {
            double positionX = node.getPositionX();
            double positionY = node.getPositionY();
            String value = node.getName();
            String shape = controller.determineElementShape(node.getProvenanceElementType());

            mxGraph graph = controller.getGraph();
            graph.insertVertex(graph.getDefaultParent(), node.getId(), value,
                    positionX, positionY, 80, 80, shape);
        }
    }

    private void createRelationshipsFromModel(ProvenanceGraph provenanceGraph) {
        for (ProvenanceNode node : provenanceGraph.getVertices()) {
            for (String target : node.getTargets()) {
                mxCell source = getMXCellById(node.getId());
                mxCell targetCell = getMXCellById(target);
                mxGraph graph = controller.getGraph();
                ProvenanceNode targetNode = controller.getProvenanceNodeById(provenanceGraph, target);
                ProvenanceRelationship provenanceRelationshipFromSourceAndTarget = controller.getProvenanceRelationshipFromSourceAndTarget(node, targetNode);
                graph.insertEdge(graph.getDefaultParent(), provenanceRelationshipFromSourceAndTarget.getId(), "", source, targetCell);
            }
        }
    }

    private mxCell getMXCellById(String ID) {
        mxCell test;
        mxGraph graph = controller.getGraph();
        graph.selectAll();
        for (Object object : graph.getSelectionCells()) {
            mxCell cell = (mxCell) object;
            if (cell.isVertex()) {
                if (cell.getId().equals(ID)) {
                    test = cell;
                    graph.clearSelection();
                    return test;
                }
            }
        }

        return null;
    }

    private void parseFileToProvenanceModel(String filePath) throws IOException, ParseException {
        JSONObject graphObject = createJsonObject(filePath);
        JSONArray nodeList = (JSONArray) graphObject.get("Nodes");
        ProvenanceGraph provenanceGraph = new ProvenanceGraph();
        evaluateJsonArray(nodeList, provenanceGraph);
        controller.addRelationshipToProvenanceGraph(provenanceGraph);
    }

    private JSONObject createJsonObject(String fileName) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = new File(fileName);
        Object graph = parser.parse(new FileReader(file));
        return (JSONObject) graph;
    }


    public void loadGraphFromFile() {
        File defaultFolder = new File("src/main/resources");
        JFileChooser fileChooser = new JFileChooser(defaultFolder.getAbsolutePath());
        FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("Json files", "json");
        fileChooser.setFileFilter(fileNameExtensionFilter);
        int returnVal = fileChooser.showOpenDialog(controller.getMainView());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                loadGraph(fileChooser.getSelectedFile().getPath());
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
        }
    }

    //FIXME: useless throw declaration
    public void saveGraphToFile() throws IOException, ParseException {
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("Json files", ".json");
        fileChooser.setFileFilter(fileNameExtensionFilter);
        fileChooser.showSaveDialog(controller.getMainView());

        ProvenanceGraph provenanceGraph = provenanceModel.getProvenanceGraphs().get(0);
        String path = fileChooser.getSelectedFile().getPath();
        saveGraph(provenanceGraph, path);
    }


    private void createProvenanceNodesFromJson(JSONArray nodeList, ProvenanceGraph provenanceGraph) {
        for (Object nodeObject : nodeList) {
            JSONObject node = (JSONObject) nodeObject;
            ProvenanceNode provenanceNode = createProvenanceNode(node);
            provenanceGraph.getVertices().add(provenanceNode);
        }
    }


    private ProvenanceNode createProvenanceNode(JSONObject node) {
        String name = node.get("name").toString();
        String id = node.get("id").toString();
        ProvenanceElementType provenanceElementType = controller.stringToProvenanceElementType(node.get("type")
                .toString());
        double coordinateX = (double) node.get("coordinateX");
        double coordinateY = (double) node.get("coordinateY");

        ProvenanceNode provenanceNode = new ProvenanceNode(id, name, provenanceElementType, coordinateX, coordinateY);
        String description = node.get("description").toString();
        provenanceNode.setDescription(description);
        JSONArray edges = (JSONArray) node.get("EdgeTarget");
        DatabaseController databaseController = controller.getDatabaseController();
        databaseController.createNewNodeInDatabase(id, provenanceElementType, name, provenanceNode);
        controller.getDirectedGraph().addVertex(id);

        for (Object nodeObject : edges) {
            JSONObject edge = (JSONObject) nodeObject;
            provenanceNode.getTargets().add(edge.get("target").toString());
        }
        return provenanceNode;
    }

    private void evaluateJsonArray(JSONArray nodeList, ProvenanceGraph provenanceGraph) {
        createProvenanceNodesFromJson(nodeList, provenanceGraph);
        createProvenanceRelationshipsFromJson(nodeList, provenanceGraph);

    }

    private void createProvenanceRelationshipsFromJson(JSONArray nodeList, ProvenanceGraph provenanceGraph) {
        for (JSONObject node : (Iterable<JSONObject>) nodeList) {
            JSONArray edges = (JSONArray) node.get("EdgeTarget");
            for (Object edge : edges) {
                addProvenanceRelationship(provenanceGraph, (JSONObject) edge);
            }
        }
    }

    private void addProvenanceRelationship(ProvenanceGraph provenanceGraph, JSONObject edge) {
        String relationshipId = edge.get("id").toString();
        String source = edge.get("source").toString();
        ProvenanceNode sourceNode = controller.getProvenanceNodeById(provenanceGraph, source);
        String target = edge.get("target").toString();
        ProvenanceNode targetNode = controller.getProvenanceNodeById(provenanceGraph, target);
        boolean checkCycle = controller.addEdgeToJGraphAndCheckForCycles(sourceNode, targetNode);
        if (checkCycle) {
            JOptionPane.showMessageDialog(controller.getMainView(), "Cycle detected, aborting loading instance", "Cycle", JOptionPane.ERROR_MESSAGE);
            controller.createNewProvenanceModel();
        }
        RelationshipType relationshipType = controller.determineRelationshipType(sourceNode, targetNode);
        ProvenanceRelationship provenanceRelationship = new ProvenanceRelationship(sourceNode, targetNode, relationshipId, relationshipType);
        DatabaseController databaseController = controller.getDatabaseController();
        GraphDatabaseService provenanceDatabase = provenanceModel.getProvenanceDatabase();
        sourceNode.getProvenanceRelationships().add(provenanceRelationship);
        databaseController.createNewRelationshipInDatabase(sourceNode, targetNode, relationshipType, provenanceDatabase);

    }

}
