package controller.editor.subcontroller;

import controller.MainController;
import controller.editor.ProvenanceEditorController;
import model.ProvenanceModel;
import model.graph.ProvenanceGraph;
import model.graph.ProvenanceNode;
import model.graph.ProvenanceRelationship;
import org.openprovenance.model.*;
import org.openprovenance.model.Process;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Marcus on 21.02.2018.
 */
//FIXME: why dont you use here sub-classing?

public class XmlExportController extends MainController {
    private ProvenanceModel provenanceModel;
    private ProvenanceEditorController controller;

    public XmlExportController(ProvenanceModel provenanceModel, ProvenanceEditorController controller) {
        this.provenanceModel = provenanceModel;
        this.controller = controller;
    }

    public void createProvenanceXMLFile(ProvenanceModel provenanceModel) {
        OPMFactory opmFactory = new OPMFactory();
        Collection<Account> accounts = Collections.singleton(opmFactory.newAccount("black"));
        ProvenanceGraph provenanceGraph = provenanceModel.getProvenanceGraphs().get(0);

        Process[] processes = fillProcessesFromProvenanceGraphToArray(provenanceGraph, opmFactory, accounts);
        Artifact[] artifacts = fillArtifactsFromProvenanceGraphToArray(provenanceGraph, opmFactory, accounts);
        Agent[] agents = fillAgentsFromProvenanceGraphToArray(provenanceGraph, opmFactory, accounts);
        Object[] dependencies = fillRelationshipsFromProvenanceGraphToArray(provenanceGraph,
                opmFactory, accounts, processes, artifacts, agents);

        OPMGraph graph = opmFactory.newOPMGraph(accounts,
                new Overlaps[]{}, processes, artifacts, agents, dependencies);

        createXMLFile(graph);

    }

    private void createXMLFile(OPMGraph graph) {
        OPMSerialiser serial = OPMSerialiser.getThreadOPMSerialiser();
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("XML files", "xml");
        fileChooser.setFileFilter(fileNameExtensionFilter);
        fileChooser.showSaveDialog(mainView);
        try {
            serial.serialiseOPMGraph(new File(fileChooser.getSelectedFile().getPath() + ".xml"), graph, true);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private Process[] fillProcessesFromProvenanceGraphToArray(ProvenanceGraph provenanceGraph, OPMFactory opmFactory, Collection<Account> accounts) {
        ArrayList<Process> processes = new ArrayList<>();
        for (ProvenanceNode provenanceNode : provenanceGraph.getVertices()) {
            if (provenanceNode.getProvenanceElementType().toString().equals("Process")) {
                String id = provenanceNode.getId();
                String name = provenanceNode.getName();
                Process process = opmFactory.newProcess(id, accounts, name);
                processes.add(process);
            }
        }

        return transformProcessListToArray(processes);
    }

    private Process[] transformProcessListToArray(ArrayList<Process> processes) {
        Process[] processArray = new Process[processes.size()];
        for (int i = 0; i < processArray.length; i++) {
            processArray[i] = processes.get(i);
        }
        return processArray;
    }

    private Artifact[] fillArtifactsFromProvenanceGraphToArray(ProvenanceGraph provenanceGraph, OPMFactory opmFactory, Collection<Account> accounts) {
        ArrayList<Artifact> artifacts = new ArrayList<>();
        for (ProvenanceNode provenanceNode : provenanceGraph.getVertices()) {
            if (provenanceNode.getProvenanceElementType().toString().equals("Artifact")) {
                String id = provenanceNode.getId();
                String name = provenanceNode.getName();
                Artifact artifact = opmFactory.newArtifact(id, accounts, name);
                artifacts.add(artifact);
            }
        }

        return transformArtifactListToArray(artifacts);
    }

    private Artifact[] transformArtifactListToArray(ArrayList<Artifact> artifacts) {
        Artifact[] artifactArray = new Artifact[artifacts.size()];
        for (int i = 0; i < artifactArray.length; i++) {
            artifactArray[i] = artifacts.get(i);
        }
        return artifactArray;
    }

    private Agent[] fillAgentsFromProvenanceGraphToArray(ProvenanceGraph provenanceGraph, OPMFactory opmFactory, Collection<Account> accounts) {
        ArrayList<Agent> agents = new ArrayList<>();
        for (ProvenanceNode provenanceNode : provenanceGraph.getVertices()) {
            if (provenanceNode.getProvenanceElementType().toString().equals("Agent")) {
                String id = provenanceNode.getId();
                String name = provenanceNode.getName();
                Agent agent = opmFactory.newAgent(id, accounts, name);
                agents.add(agent);
            }
        }

        return transformAgentListToArray(agents);
    }

    private Agent[] transformAgentListToArray(ArrayList<Agent> agents) {
        Agent[] agentArray = new Agent[agents.size()];
        for (int i = 0; i < agentArray.length; i++) {
            agentArray[i] = agents.get(i);
        }
        return agentArray;
    }

    private Object[] fillRelationshipsFromProvenanceGraphToArray(ProvenanceGraph provenanceGraph, OPMFactory opmFactory,
                                                                 Collection<Account> accounts, Process[] processes,
                                                                 Artifact[] artifacts, Agent[] agents) {
        ArrayList<Object> relationships = new ArrayList<>();
        for (ProvenanceNode provenanceNode : provenanceGraph.getVertices()) {
            for (ProvenanceRelationship provenanceRelationship : provenanceNode.getProvenanceRelationships()) {

                String idSource = provenanceRelationship.getSourceProvenanceNode().getId();
                String idTarget = provenanceRelationship.getTargetProvenanceNode().getId();
                String dependencyId = provenanceRelationship.getId();

                switch (provenanceRelationship.getRelationshipType()) {
                    case USED: {
                        createUsedDependency(opmFactory, accounts, processes, artifacts, relationships, idSource, idTarget, dependencyId);
                    }
                    break;
                    case WAS_DERIVED_FROM: {
                        createWasDerivedFromDependency(opmFactory, accounts, artifacts, relationships, idSource, idTarget);
                    }
                    break;
                    case WAS_GENERATED_BY: {
                        createWasGeneratedByDependency(opmFactory, accounts, processes, artifacts, relationships, idSource, idTarget, dependencyId);
                    }
                    break;
                    case WAS_TRIGGERED_BY: {
                        createWasTriggeredByDependency(opmFactory, accounts, processes, relationships, idSource, idTarget);
                    }
                    break;
                    case WAS_CONTROLLED_BY: {
                        createWasControlledByDependency(opmFactory, accounts, processes, agents, relationships, idSource, dependencyId);
                    }
                }
            }
        }

        return relationships.toArray();
    }

    private void createWasControlledByDependency(OPMFactory opmFactory, Collection<Account> accounts, Process[] processes, Agent[] agents, ArrayList<Object> relationships, String idSource, String dependencyId) {
        Process processSource = getProcessById(processes, idSource);
        Agent agentTarget = getAgentById(agents, dependencyId);
        WasControlledBy wasControlledBy = opmFactory.newWasControlledBy(processSource, opmFactory.newRole(dependencyId), agentTarget, accounts);
        relationships.add(wasControlledBy);
    }

    private void createWasTriggeredByDependency(OPMFactory opmFactory, Collection<Account> accounts, Process[] processes, ArrayList<Object> relationships, String idSource, String idTarget) {
        Process processSource = getProcessById(processes, idSource);
        Process processTarget = getProcessById(processes, idTarget);
        WasTriggeredBy wasTriggeredBy = opmFactory.newWasTriggeredBy(processSource, processTarget, accounts);
        relationships.add(wasTriggeredBy);
    }

    private void createWasGeneratedByDependency(OPMFactory opmFactory, Collection<Account> accounts, Process[] processes, Artifact[] artifacts, ArrayList<Object> relationships, String idSource, String idTarget, String dependencyId) {
        Artifact artifactSource = getArtifactById(artifacts, idSource);
        Process processTarget = getProcessById(processes, idTarget);
        WasGeneratedBy wasGeneratedBy = opmFactory.newWasGeneratedBy(artifactSource, opmFactory.newRole(dependencyId),
                processTarget, accounts);
        relationships.add(wasGeneratedBy);
    }

    private void createWasDerivedFromDependency(OPMFactory opmFactory, Collection<Account> accounts, Artifact[] artifacts, ArrayList<Object> relationships, String idSource, String idTarget) {
        Artifact artifactTarget = getArtifactById(artifacts, idTarget);
        Artifact artifactSource = getArtifactById(artifacts, idSource);
        WasDerivedFrom wasDerivedFrom = opmFactory.newWasDerivedFrom(artifactSource
                , artifactTarget, accounts);
        relationships.add(wasDerivedFrom);
    }

    private void createUsedDependency(OPMFactory opmFactory, Collection<Account> accounts, Process[] processes, Artifact[] artifacts, ArrayList<Object> relationships, String idSource, String idTarget, String dependencyId) {
        Artifact artifactTarget = getArtifactById(artifacts, idTarget);
        Process processSource = getProcessById(processes, idSource);
        Used used = opmFactory.newUsed(processSource,
                opmFactory.newRole(dependencyId), artifactTarget, accounts);
        relationships.add(used);
    }

    private Process getProcessById(Process[] processes, String id) {
        for (Process process : processes) {
            if (process.getId().equals(id)) {
                return process;
            }
        }

        return null;
    }

    private Artifact getArtifactById(Artifact[] artifacts, String id) {
        for (Artifact artifact : artifacts) {
            if (artifact.getId().equals(id)) {
                return artifact;
            }
        }
        return null;
    }

    private Agent getAgentById(Agent[] agents, String id) {
        for (Agent agent : agents) {
            if (agent.equals(id)) {
                return agent;
            }
        }
        return null;
    }


}
