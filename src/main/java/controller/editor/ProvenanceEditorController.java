package controller.editor;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import controller.MainController;
import controller.editor.subcontroller.DatabaseController;
import controller.editor.subcontroller.FileManagementController;
import controller.editor.subcontroller.InformationViewController;
import controller.editor.subcontroller.XmlExportController;
import model.ProvenanceModel;
import model.graph.*;
import model.graph.RelationshipType;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.neo4j.graphdb.*;
import view.MainView;
import view.information.graphelements.NodeView;
import view.information.graphelements.RelationshipView;
import view.provenance.ProvenanceModelView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;


public class ProvenanceEditorController extends MainController {
    private String currentFileName;
    private ProvenanceElementType dragElementType;
    private ProvenanceModel provenanceModel;
    private mxGraph graph;
    private DirectedGraph<String, DefaultEdge> directedGraph;
    private XmlExportController xmlExportController;
    private InformationViewController informationViewController;
    private DatabaseController databaseController;
    private FileManagementController fileManagementController;

    public ProvenanceEditorController(ProvenanceModel provenanceModel) {
        this.provenanceModel = provenanceModel;
        this.directedGraph = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
        this.mainView = new MainView(this);


        this.xmlExportController = new XmlExportController(provenanceModel, this);
        this.informationViewController = new InformationViewController(provenanceModel, this);
        this.databaseController = new DatabaseController(provenanceModel, this);
        this.fileManagementController = new FileManagementController(provenanceModel, this);
    }

    public DirectedGraph<String, DefaultEdge> getDirectedGraph() {
        return directedGraph;
    }

    public void setCurrentFileName(String currentFileName) {
        this.currentFileName = currentFileName;
    }

    public mxGraph getGraph() {
        return graph;
    }

    public void setGraph(mxGraph graph) {
        this.graph = graph;
    }

    public DatabaseController getDatabaseController() {
        return databaseController;
    }

    public InformationViewController getInformationViewController() {
        return informationViewController;
    }

    public void showView() {
        this.mainView.run();
    }

    public void createNewProvenanceModel() {
        ProvenanceModelView provenanceModelView = new ProvenanceModelView(this);
        JPanel mainPanel = mainView.getMainPanel();
        directedGraph = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
        addNewProvenanceModelToPanel(provenanceModelView, mainPanel);
    }

    private void addNewProvenanceModelToPanel(ProvenanceModelView provenanceModelView, JPanel mainPanel) {
        mainPanel.remove(((BorderLayout) mainPanel.getLayout()).getLayoutComponent(BorderLayout.CENTER));
        provenanceModelView.setBorder(BorderFactory.createTitledBorder("Provenance-Editor"));
        mainPanel.add(provenanceModelView, BorderLayout.CENTER);
        clearInformationView();

        mainPanel.revalidate();
        mainPanel.repaint();
    }

    private void clearInformationView() {
        NodeView nodeView = mainView.getInformationView().getNodeView();
        RelationshipView relationshipView = mainView.getInformationView().getRelationshipView();
        informationViewController.clearNodeView(nodeView);
        informationViewController.clearEdgeView(relationshipView);
        nodeView.setVisible(false);
        relationshipView.setVisible(false);
    }

    public void drawProvenanceElement(mxGraphComponent targetPanel, double positionX, double positionY) {
        String elementShape = determineElementShape(dragElementType);
        insertVertexIntoGraph(targetPanel, positionX, positionY, elementShape);
        targetPanel.revalidate();
        targetPanel.repaint();
    }

    private void insertVertexIntoGraph(mxGraphComponent targetPanel, double positionX, double positionY, String elementShape) {
        mxGraph graph = targetPanel.getGraph();
        graph.getModel().beginUpdate();
        String cellValue = setValueInputDialog();
        mxCell cell = (mxCell) graph.insertVertex(graph.getDefaultParent(), "1", cellValue,
                positionX, positionY, 80, 80, elementShape);
        createNewProvenanceNode(cell);
        graph.getModel().endUpdate();

    }

    private String setValueInputDialog() {
        return JOptionPane.showInputDialog(mainView, "Name the Element");
    }

    public String determineElementShape(ProvenanceElementType provenanceElementType) {
        String elementShape = "";
        switch (provenanceElementType) {
            case Process:
                break;
            case Artifact:
                elementShape = "shape=ellipse";
                break;
            case Agent:
                elementShape = "shape=hexagon";
                break;
        }
        return elementShape;
    }

    public void setDragElementType(ProvenanceElementType dragElementType) {
        this.dragElementType = dragElementType;
    }

    /**
     * Adds a dependency to the provenance graph
     * @param graph
     * @param cell
     */
    public void addRelationshipToProvenanceGraph(mxGraph graph, mxCell cell) {
        ProvenanceGraph provenanceGraph = provenanceModel.getProvenanceGraphs().get(0);
        if (cell.isEdge()) {
            if (cell.getTarget() == null) {
                graph.getModel().remove(cell);
            } else {
                createNewProvenanceRelationship(graph, provenanceGraph, cell);
            }
        }

    }

    private void createNewProvenanceRelationship(mxGraph graph, ProvenanceGraph provenanceGraph, mxCell element) {
        ProvenanceNode sourceNode = determineRelationshipSource(element, provenanceGraph);
        ProvenanceNode targetNode = determineRelationshipTarget(element, provenanceGraph);
        RelationshipType relationshipType = determineRelationshipType(sourceNode, targetNode);
        GraphDatabaseService provenanceDatabase = provenanceModel.getProvenanceDatabase();
        if (relationshipType == RelationshipType.NONE) {
            JOptionPane.showMessageDialog(mainView, "This is not a valid relationship", "Not a valid Relationship", JOptionPane.ERROR_MESSAGE);
            graph.getModel().remove(element);
        } else if (addEdgeToJGraphAndCheckForCycles(sourceNode, targetNode)) {
            graph.getModel().remove(element);
            JOptionPane.showMessageDialog(mainView, "You just created a cycle, which is not allowed in an OPM Graph", "Cycle detected", JOptionPane.ERROR_MESSAGE);
        } else {
            String id = element.getId();
            ProvenanceRelationship relationship = new ProvenanceRelationship(sourceNode, targetNode, id, relationshipType);
            sourceNode.getProvenanceRelationships().add(relationship);
            addEdgeToJGraphAndCheckForCycles(sourceNode, targetNode);
            databaseController.createNewRelationshipInDatabase(sourceNode, targetNode, relationshipType, provenanceDatabase);
        }
    }

    /**
     * This method checks if the provenance graph remains acyclic
     * @param sourceNode
     * @param targetNode
     * @return Check if model contains cycles
     */
    public boolean addEdgeToJGraphAndCheckForCycles(ProvenanceNode sourceNode, ProvenanceNode targetNode) {
        String sourceNodeId = sourceNode.getId();
        String targetNodeId = targetNode.getId();
        DefaultEdge defaultEdge = directedGraph.addEdge(sourceNodeId, targetNodeId);
        CycleDetector<String, DefaultEdge> cycleDetector = new CycleDetector<>(directedGraph);
        if (cycleDetector.detectCycles()) {
            directedGraph.removeEdge(defaultEdge);
            return true;
        } else {
            return false;
        }
    }

    private void createNewProvenanceNode(mxCell cell) {
        if (cell.isVertex()) {
            String nodeId = cell.getId();
            ProvenanceElementType provenanceElementType = determineElementType(cell);
            String nodeName = cell.getValue().toString();
            ProvenanceNode provenanceNode = new ProvenanceNode(nodeId, nodeName, provenanceElementType, cell.getGeometry()
                    .getX(), cell.getGeometry().getY());
            ProvenanceGraph provenanceGraph = provenanceModel.getProvenanceGraphs().get(0);
            provenanceGraph.getVertices().add(provenanceNode);
            directedGraph.addVertex(nodeId);
            databaseController.createNewNodeInDatabase(nodeId, provenanceElementType, nodeName, provenanceNode);
        }

    }

    public RelationshipType determineRelationshipType(ProvenanceNode sourceNode, ProvenanceNode targetNode) {
        ProvenanceElementType sourceType = sourceNode.getProvenanceElementType();
        ProvenanceElementType targetType = targetNode.getProvenanceElementType();

        System.out.println(sourceType.toString() + "    " + targetType.toString());

        RelationshipType relationshipType = RelationshipType.NONE;
        switch (sourceType) {
            case Process: {
                switch (targetType) {
                    case Process: {
                        return RelationshipType.WAS_TRIGGERED_BY;

                    }
                    case Agent: {
                        return RelationshipType.WAS_CONTROLLED_BY;
                    }
                    case Artifact: {
                        return RelationshipType.USED;

                    }
                }
            }
            case Artifact: {
                switch (targetType) {
                    case Process: {
                        return RelationshipType.WAS_GENERATED_BY;

                    }
                    case Artifact: {
                        return RelationshipType.WAS_DERIVED_FROM;

                    }
                    case Agent: {
                        return relationshipType;
                    }
                }
            }
            case Agent: {
                return relationshipType;
            }
        }

        return relationshipType;
    }

    private ProvenanceNode determineRelationshipSource(mxCell cell, ProvenanceGraph provenanceGraph) {
        ProvenanceNode provenanceNode = null;
        if (cell.isEdge()) {
            provenanceNode = getProvenanceNodeById(provenanceGraph, cell.getSource().getId());
        }
        return provenanceNode;
    }

    private ProvenanceNode determineRelationshipTarget(mxCell cell, ProvenanceGraph provenanceGraph) {
        ProvenanceNode provenanceNode = null;
        if (cell.isEdge()) {
            provenanceNode = getProvenanceNodeById(provenanceGraph, cell.getTarget().getId());
        }
        return provenanceNode;
    }

    public ProvenanceNode getProvenanceNodeById(ProvenanceGraph provenanceGraph, String id) {
        ProvenanceNode provenanceNode = null;
        for (ProvenanceNode nodes : provenanceGraph.getVertices()) {
            if (nodes.getId().equals(id)) {
                provenanceNode = nodes;
            }
        }
        return provenanceNode;
    }

    public ProvenanceRelationship getProvenanceRelationshipById(ProvenanceGraph provenanceGraph, String id) {
        for (ProvenanceNode provenanceNode : provenanceGraph.getVertices()) {
            for (ProvenanceRelationship provenanceRelationship1 : provenanceNode.getProvenanceRelationships()) {
                if (provenanceRelationship1.getId().equals(id)) {
                    return provenanceRelationship1;
                }
            }
        }
        return null;
    }

    private ProvenanceElementType determineElementType(mxCell cell) {
        String cellType = cell.getStyle();
        ProvenanceElementType provenanceElementType;
        if (cellType.contains("ellipse")) {
            provenanceElementType = ProvenanceElementType.Artifact;
        } else if (cellType.contains("hexagon")) {
            provenanceElementType = ProvenanceElementType.Agent;
        } else {
            provenanceElementType = ProvenanceElementType.Process;
        }
        return provenanceElementType;
    }

    public void createNewProvenanceGraph(ProvenanceModel provenanceModel) {
        ProvenanceGraph provenanceGraph = new ProvenanceGraph();
        provenanceModel.getProvenanceGraphs().add(provenanceGraph);
        provenanceModel.getProvenanceDatabase().execute("Match (n) Detach Delete n");
        //directedGraph = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
    }


    public void addRelationshipToProvenanceGraph(ProvenanceGraph provenanceGraph) {
        provenanceModel.getProvenanceGraphs().removeAll(provenanceModel.getProvenanceGraphs());
        provenanceModel.getProvenanceGraphs().add(provenanceGraph);
    }


    public ProvenanceElementType stringToProvenanceElementType(String elementType) {
        if (elementType.equals("Process")) {
            return ProvenanceElementType.Process;
        } else if (elementType.equals("Artifact")) {
            return ProvenanceElementType.Artifact;
        } else {
            return ProvenanceElementType.Agent;
        }
    }

    public void deleteElements(mxGraph graph, KeyEvent keyEvent) {
        ProvenanceGraph provenanceGraph = provenanceModel.getProvenanceGraphs().get(0);
        if (keyEvent.getKeyCode() == KeyEvent.VK_DELETE) {
            Object[] selectionCells = graph.getSelectionCells();
            for (Object object : selectionCells) {
                deleteElements(provenanceGraph, (mxCell) object);
            }
            graph.removeCells(selectionCells);
        }
    }

    private void deleteElements(ProvenanceGraph provenanceGraph, mxCell cell) {
        databaseController.deleteElementsFromDatabase(provenanceGraph, cell);
        deleteElementFromProvenanceModel(provenanceGraph, cell);
        if (cell.isVertex()) {
            directedGraph.removeVertex(cell.getId());
        } else if (cell.isEdge()) {
            directedGraph.removeEdge(cell.getSource().getId(), cell.getTarget().getId());
        }
    }

    private void deleteElementFromProvenanceModel(ProvenanceGraph provenanceGraph, mxCell cell) {
        if (cell.isVertex()) {
            deleteNodeFromGraph(provenanceGraph, cell);
        } else if (cell.isEdge()) {
            deleteEdgeFromGraph(provenanceGraph, cell);
        }
    }

    private void deleteEdgeFromGraph(ProvenanceGraph provenanceGraph, mxCell cell) {
        ProvenanceRelationship provenanceRelationshipById = getProvenanceRelationshipById(provenanceGraph, cell.getId());
        ProvenanceNode sourceProvenanceNode = provenanceRelationshipById.getSourceProvenanceNode();
        sourceProvenanceNode.getProvenanceRelationships().remove(provenanceRelationshipById);
    }

    private void deleteNodeFromGraph(ProvenanceGraph provenanceGraph, mxCell cell) {
        ProvenanceNode provenanceNodeById = getProvenanceNodeById(provenanceGraph, cell.getId());
        provenanceGraph.getVertices().remove(provenanceNodeById);
    }

    public void handleDescriptionChanges() {
        NodeView nodeView = mainView.getInformationView().getNodeView();
        JTextArea descriptionTextArea = nodeView.getDescriptionTextArea();
        JTextField idTextfield = nodeView.getIdTextfield();
        String jsonSpecification = descriptionTextArea.getText();
        ProvenanceGraph provenanceGraph = getProvenanceModel().getProvenanceGraphs().get(0);
        String Id = idTextfield.getText();
        ProvenanceNode provenanceNode = getProvenanceNodeById(provenanceGraph, Id);
        provenanceNode.setDescription(jsonSpecification);
        JSONParser parser = new JSONParser();
        try {
            JSONObject JSONObject = (JSONObject) parser.parse("{" + jsonSpecification + " }");
            for (Object description : JSONObject.entrySet()) {
                Map.Entry pair = (Map.Entry) description;
                String key = pair.getKey().toString();
                String value = pair.getValue().toString();
                ArrayList<String> descriptionList = new ArrayList<String>();
                String[] values = value.split(",");
                descriptionList.addAll(Arrays.asList(values));
                provenanceNode.getDescriptionMap().put(key, descriptionList);
                System.out.println("Key : " + key + " Value " + descriptionList.toString());

                databaseController.addDescriptionToDatabase(Id, key, descriptionList);
            }
        } catch (ParseException parseException) {
            String title = "Incorrect JSON Syntax";
            MainView mainView = getMainView();
            JOptionPane.showMessageDialog(mainView, parseException.getMessage(), title, JOptionPane.ERROR_MESSAGE);
            parseException.printStackTrace();
        }
    }


    public ProvenanceRelationship getProvenanceRelationshipFromSourceAndTarget(ProvenanceNode source, ProvenanceNode target) {
        for (ProvenanceRelationship relationship : source.getProvenanceRelationships()) {
            if (relationship.getTargetProvenanceNode() == target) {
                return relationship;
            }
        }
        return null;
    }


    public ProvenanceModel getProvenanceModel() {
        return provenanceModel;
    }

    public XmlExportController getXmlExportController() {
        return xmlExportController;
    }

    public FileManagementController getFileManagementController() {
        return fileManagementController;
    }

    public void layoutGraph() {
        new mxHierarchicalLayout(this.graph).execute(this.graph.getDefaultParent());
    }
}
