package view.query;

import controller.editor.ProvenanceEditorController;

import javax.swing.*;

/**
 * Created by Marcus on 19.02.2018.
 */
public class QueryView extends JPanel {

    private ProvenanceEditorController controller;
    private QueryInputView queryInputView;
    private QueryResultLogView queryResultLogView;

    public QueryView(ProvenanceEditorController controller) {
        this.controller = controller;
        queryInputView = new QueryInputView(controller);
        queryResultLogView = new QueryResultLogView();


        setup();
    }

    public QueryInputView getQueryInputView() {
        return queryInputView;
    }

    public QueryResultLogView getQueryResultLogView() {
        return queryResultLogView;
    }

    private void setup() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createTitledBorder("Query"));
        this.add(queryInputView);
        this.add(queryResultLogView);
    }
}
