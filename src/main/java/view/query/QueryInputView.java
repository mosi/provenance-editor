package view.query;

import controller.editor.ProvenanceEditorController;
import controller.editor.subcontroller.DatabaseController;

import javax.swing.*;

/**
 * Created by Marcus on 08.02.2018.
 */
public class QueryInputView extends JPanel {
    private JTextArea cypherTextArea;
    private ProvenanceEditorController controller;
    private JButton executeButton;

    public QueryInputView(ProvenanceEditorController controller) {
        this.controller = controller;
        this.cypherTextArea = new JTextArea(15, 15);
        this.executeButton = new JButton("Execute Query");

        setup();
    }

    private void setup() {
        this.setBorder(BorderFactory.createTitledBorder("Query - Input"));
        JScrollPane scrollPane = new JScrollPane(cypherTextArea);
        cypherTextArea.setFont(cypherTextArea.getFont().deriveFont(18f));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(scrollPane);

        executeButton.addActionListener(e -> {
            DatabaseController databaseController = controller.getDatabaseController();
            databaseController.handleExecuteQueryButton(cypherTextArea);
        });
        this.add(executeButton);


    }

    public JTextArea getCypherTextArea() {
        return cypherTextArea;
    }
}
