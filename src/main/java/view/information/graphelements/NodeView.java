package view.information.graphelements;

import controller.editor.ProvenanceEditorController;
import model.graph.ProvenanceGraph;
import model.graph.ProvenanceNode;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import view.MainView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by Marcus on 14.12.2017.
 */
public class NodeView extends JPanel {
    private static final int TEXT_LENGHT = 30;
    private JLabel idLabel;
    private JTextField idTextfield;
    private JLabel typeLabel;
    private JTextField typeTextField;
    private JLabel descriptionLabel;
    private JTextArea descriptionTextArea;
    private JLabel relationshipLabel;
    private JTextField relationshipTextField;

    private ProvenanceEditorController controller;

    public NodeView(ProvenanceEditorController controller) {
        this.idLabel = new JLabel("ID: ");
        this.idTextfield = new JTextField(TEXT_LENGHT);
        this.typeLabel = new JLabel("Type: ");
        this.typeTextField = new JTextField(TEXT_LENGHT);
        this.descriptionLabel = new JLabel("Description: ");
        this.descriptionTextArea = new JTextArea(20, 10);
        this.relationshipLabel = new JLabel("Relationships with :");
        this.relationshipTextField = new JTextField(TEXT_LENGHT);

        this.controller = controller;
        setup();
    }

    public JTextField getRelationshipTextField() {
        return relationshipTextField;
    }

    public JTextField getTypeTextField() {
        return typeTextField;
    }


    public JTextArea getDescriptionTextArea() {
        return descriptionTextArea;
    }


    private void setup() {
        this.setVisible(false);
        this.setLayout(new GridBagLayout());

        Font serif = new Font("Arial", Font.BOLD, 18);

        idLabel.setFont(serif);
        typeLabel.setFont(serif);
        descriptionLabel.setFont(serif);
        relationshipLabel.setFont(serif);

        idTextfield.setEditable(false);
        typeTextField.setEditable(false);
        relationshipTextField.setEditable(false);
        descriptionTextArea.setFont(serif);

        descriptionTextArea.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                controller.handleDescriptionChanges();
            }
        });

        JScrollPane scrollPane = new JScrollPane(descriptionTextArea);
        this.add(idLabel, fillElementIntoGridLayout(0, 0));
        this.add(idTextfield, fillElementIntoGridLayout(1, 0));
        this.add(typeLabel, fillElementIntoGridLayout(0, 1));
        this.add(typeTextField, fillElementIntoGridLayout(1, 1));
        this.add(relationshipLabel, fillElementIntoGridLayout(0, 2));
        this.add(relationshipTextField, fillElementIntoGridLayout(1, 2));
        this.add(descriptionLabel, fillElementIntoGridLayout(0, 3));
        this.add(scrollPane, fillElementIntoGridLayout(1, 3));


    }

    private GridBagConstraints fillElementIntoGridLayout(int column, int row) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = column;
        gridBagConstraints.gridy = row;

        return gridBagConstraints;
    }

    public JTextField getIdTextfield() {
        return idTextfield;
    }

}
