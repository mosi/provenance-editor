package view.information;

import controller.editor.ProvenanceEditorController;
import view.information.graphelements.NodeView;
import view.information.graphelements.RelationshipView;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Marcus on 29.10.2017.
 */
public class InformationView extends JPanel {

    private ProvenanceEditorController controller;
    private NodeView nodeView;
    private RelationshipView relationshipView;

    public InformationView(ProvenanceEditorController controller) {

        this.controller = controller;
        this.nodeView = new NodeView(controller);
        this.relationshipView = new RelationshipView();
        setup();
    }

    public NodeView getNodeView() {
        return nodeView;
    }

    public RelationshipView getRelationshipView() {
        return relationshipView;
    }

    private void setup() {
        this.setBorder(BorderFactory.createTitledBorder("Element Information"));
        this.setLayout(new BorderLayout());

        this.add(nodeView, BorderLayout.WEST);
        this.add(relationshipView, BorderLayout.WEST);

    }


}
