package view.provenance;


import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxEvent;
import com.mxgraph.view.mxGraph;
import controller.editor.ProvenanceEditorController;
import controller.editor.subcontroller.InformationViewController;
import model.graph.ProvenanceGraph;
import model.graph.ProvenanceNode;
import model.graph.ProvenanceRelationship;
import view.information.InformationView;
import view.interaction.interactionelements.ElementDropTargetListener;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Created by Marcus on 02.11.2017.
 */
public class ProvenanceModelView extends JPanel {

    private ProvenanceEditorController controller;

    private JPopupMenu popupMenu;
    private mxGraph graph;
    private mxGraphComponent graphPanel;

    public ProvenanceModelView(ProvenanceEditorController controller) {

        this.controller = controller;
        addPopupMenu();
        initiateGraph(controller);

        TransferHandler transferHandler = createTransferHandler();

        graphPanel.setTransferHandler(transferHandler);
        new ElementDropTargetListener(graphPanel, controller);

        setup();

    }

    private void setup() {

        graphPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        graphPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        graphPanel.getHorizontalScrollBar().setUnitIncrement(20);
        graphPanel.getViewport().setViewSize(new Dimension(10000, 900));
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.add(graphPanel);
    }

    private TransferHandler createTransferHandler() {
        return new TransferHandler() {
            @Override
            public boolean canImport(TransferSupport support) {
                return support.isDrop() && support.isDataFlavorSupported(DataFlavor.imageFlavor);

            }

            @Override
            public boolean importData(TransferSupport support) {
                if (!canImport(support)) {
                    return false;
                }

                Transferable transferable = support.getTransferable();
                Icon ico;
                try {
                    ico = (Icon) transferable.getTransferData(DataFlavor.imageFlavor);
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                graphPanel.add(new JLabel(ico));
                return true;
            }
        };
    }


    private void addPopupMenu() {
        popupMenu = new JPopupMenu();
        JMenuItem delete = new JMenuItem("delete");
        delete.addActionListener(e -> graph.removeCells(graph.getSelectionCells()));
        popupMenu.add(delete);
        this.add(popupMenu);
    }

    private void initiateGraph(ProvenanceEditorController controller) {
        graph = new mxGraph();
        graphPanel = new mxGraphComponent(graph);
        graphPanel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                controller.deleteElements(graph, e);
            }
        });
        graphPanel.getConnectionHandler().addListener(mxEvent.CONNECT, (o, mxEventObject) -> {
            mxCell edge = (mxCell) mxEventObject.getProperties().get("cell");
            controller.addRelationshipToProvenanceGraph(graph, edge);
        });
        graphPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        graphPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        graphPanel.getGraphControl().addMouseListener(new GraphControlListener());

        controller.setGraph(graph);
        controller.createNewProvenanceGraph(controller.getProvenanceModel());
    }

    public class GraphControlListener extends MouseAdapter {
        private Point origin;

        @Override
        public void mousePressed(MouseEvent e) {
            origin = new Point(e.getPoint());

        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (origin != null) {
                JViewport viewport = graphPanel.getViewport();
                if (viewport != null) {
                    int dx = origin.x - e.getX();
                    int dy = origin.y - e.getY();

                    Rectangle view = viewport.getViewRect();
                    view.x += dx;
                    view.y += dy;

                    graphPanel.scrollRectToVisible(view);
                }
            }

        }

        @Override
        public void mouseReleased(MouseEvent e) {
            for (Object cell : graph.getSelectionCells()) {
                mxCell node = (mxCell) cell;
                if (node != null) {
                    if (node.isVertex()) {
                        ProvenanceGraph graph = controller.getProvenanceModel().getProvenanceGraphs().get(0);
                        ProvenanceNode provenanceNode = controller.getProvenanceNodeById(graph, node.getId());
                        double positionX = provenanceNode.getPositionX();
                        double positionY = provenanceNode.getPositionY();
                        provenanceNode.setPositionX(node.getGeometry().getX());
                        provenanceNode.setPositionY(node.getGeometry().getY());
                        double positionX1 = provenanceNode.getPositionX();
                        double positionY1 = provenanceNode.getPositionY();
                        System.out.println("alt x " + positionX + " neu x " + positionX1 + "alt y " + positionY + " neu y " + positionY1);
                    }
                }

            }
        }


        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isRightMouseButton(e)) {
                popupMenu.show(graphPanel, e.getX(), e.getY());
            }
            if (SwingUtilities.isLeftMouseButton(e)) {
                mxCell cell = (mxCell) graphPanel.getCellAt(e.getX(), e.getY());
                if (cell != null) {
                    String cellId = cell.getId();
                    System.out.println(cell.getId());
                    InformationView informationView = controller.getMainView().getInformationView();
                    ProvenanceGraph provenanceGraph = controller.getProvenanceModel().getProvenanceGraphs().get(0);
                    InformationViewController informationViewController = controller.getInformationViewController();
                    if (cell.isVertex()) {
                        ProvenanceNode provenanceNode = controller.getProvenanceNodeById(provenanceGraph, cellId);
                        informationViewController.updateNodeInformationView(informationView, provenanceNode);
                    }
                    if (cell.isEdge()) {
                        ProvenanceRelationship provenanceRelationship = controller.getProvenanceRelationshipById(provenanceGraph, cellId);

                        informationViewController.updateEdgeInformationView(informationView, provenanceRelationship);
                    }
                }
            }
        }
    }
}

