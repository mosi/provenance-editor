package view;

import controller.editor.ProvenanceEditorController;
import view.information.InformationView;
import view.interaction.ProvenanceInteractionView;
import view.menu.ProvenanceEditorMenuBar;
import view.provenance.ProvenanceModelView;
import view.query.QueryView;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Marcus on 29.10.2017.
 */
public class MainView extends JFrame {

    private ProvenanceEditorMenuBar provenanceEditorMenuBar;
    private JPanel mainPanel;
    private ProvenanceModelView provenanceModelView;
    private BorderLayout provenanceEditorBorderLayout;
    private ProvenanceInteractionView provenanceInteractionView;

    private QueryView queryView;
    private ProvenanceEditorController controller;

    public MainView(ProvenanceEditorController controller) {

        this.controller = controller;
        provenanceModelView = new ProvenanceModelView(controller);
        mainPanel = new JPanel();
        provenanceEditorMenuBar = new ProvenanceEditorMenuBar(controller);
        provenanceEditorBorderLayout = new BorderLayout();
        provenanceInteractionView = new ProvenanceInteractionView(controller);
        queryView = new QueryView(controller);
        setup();
    }

    public QueryView getQueryView() {
        return queryView;
    }

    public InformationView getInformationView() {
        return provenanceInteractionView.getInformationView();
    }

    private void setup() {
        this.setSize(1920, 900);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("Provenance Editor");
        this.setResizable(false);
        this.setJMenuBar(provenanceEditorMenuBar);

        mainPanel.setLayout(provenanceEditorBorderLayout);
        provenanceModelView.setBorder(BorderFactory.createTitledBorder("Provenance-Editor"));
        mainPanel.add(provenanceModelView, BorderLayout.CENTER);

        mainPanel.add(queryView, BorderLayout.EAST);

        provenanceInteractionView.setBorder(BorderFactory.createTitledBorder("Elements"));
        mainPanel.add(provenanceInteractionView, BorderLayout.WEST);

        this.add(mainPanel);

        this.setLocationRelativeTo(null);

    }


    public void run() {
        SwingUtilities.invokeLater(() -> setup());
        this.setVisible(true);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

}
