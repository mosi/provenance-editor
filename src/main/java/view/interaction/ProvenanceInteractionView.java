package view.interaction;

import controller.editor.ProvenanceEditorController;
import view.information.InformationView;
import view.interaction.interactionelements.ProvenanceElementView;
import view.query.QueryInputView;

import javax.swing.*;

/**
 * Created by Marcus on 08.02.2018.
 */
public class ProvenanceInteractionView extends JPanel {
    private ProvenanceElementView provenanceElementView;
    private QueryInputView queryInputView;
    private JButton executeQueryButton;
    private ProvenanceEditorController controller;
    private InformationView informationView;

    public ProvenanceInteractionView(ProvenanceEditorController controller) {
        this.controller = controller;
        this.informationView = new InformationView(controller);
        this.provenanceElementView = new ProvenanceElementView(controller);
        this.queryInputView = new QueryInputView(controller);
        this.executeQueryButton = new JButton("Execute Query");

        setup();
    }

    public InformationView getInformationView() {
        return informationView;
    }

    private void setup() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(provenanceElementView);
        this.add(informationView);
        //this.add(queryInputView);
        //this.add(executeQueryButton);
    }
}
