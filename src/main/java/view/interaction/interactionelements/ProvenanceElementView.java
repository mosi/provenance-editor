package view.interaction.interactionelements;

import controller.editor.ProvenanceEditorController;

import javax.swing.*;
import java.awt.*;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;


/**
 * Created by Marcus on 02.11.2017.
 */
public class ProvenanceElementView extends JPanel {
    private ProvenanceEditorController controller;
    private JLabel process;
    private JLabel artifact;
    private JLabel agent;

    public ProvenanceElementView(ProvenanceEditorController controller) {

        this.controller = controller;

        this.process = new JLabel(new ImageIcon(getClass().getResource("/images/Process.png")));
        this.artifact = new JLabel(new ImageIcon(getClass().getResource("/images/Artifact.png")));
        this.agent = new JLabel(new ImageIcon(getClass().getResource("/images/Agent.png")));
        this.add(process);
        this.add(artifact);
        this.add(agent);

        setup();
    }

    private void setup() {
        this.setLayout(new FlowLayout());

        process.setText("Process");
        artifact.setText("Artifact");
        agent.setText("Agent");


        ElementDragGestureListener dragGestureListener = new ElementDragGestureListener(controller);
        DragSource dragSource = new DragSource();
        dragSource.createDefaultDragGestureRecognizer(agent, DnDConstants.ACTION_COPY_OR_MOVE, dragGestureListener);
        dragSource.createDefaultDragGestureRecognizer(process, DnDConstants.ACTION_COPY_OR_MOVE, dragGestureListener);
        dragSource.createDefaultDragGestureRecognizer(artifact, DnDConstants.ACTION_COPY_OR_MOVE, dragGestureListener);
    }
}
