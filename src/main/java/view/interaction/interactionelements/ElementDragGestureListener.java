package view.interaction.interactionelements;

import controller.editor.ProvenanceEditorController;
import model.graph.ProvenanceElementType;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.io.IOException;

/**
 * Created by Marcus on 16.11.2017.
 */
public class ElementDragGestureListener implements DragGestureListener {
    private ProvenanceEditorController controller;

    public ElementDragGestureListener(ProvenanceEditorController controller) {
        this.controller = controller;
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent event) {
        JLabel ProvenanceElement = (JLabel) event.getComponent();
        determineProvenanceElementType(ProvenanceElement);

        Transferable transferable = new Transferable() {
            @Override
            public DataFlavor[] getTransferDataFlavors() {
                return new DataFlavor[]{DataFlavor.imageFlavor};
            }

            @Override
            public boolean isDataFlavorSupported(DataFlavor flavor) {
                return isDataFlavorSupported(flavor);
            }

            @Override
            public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
                return ProvenanceElement.getText();
            }
        };
        event.startDrag(null, transferable);
    }

    private void determineProvenanceElementType(JLabel component) {
        String componentText = component.getText();


        switch (componentText) {
            case "Process":
                controller.setDragElementType(ProvenanceElementType.Process);
                break;
            case "Artifact":
                controller.setDragElementType(ProvenanceElementType.Artifact);
                break;
            case "Agent":
                controller.setDragElementType(ProvenanceElementType.Agent);
        }
    }
}
