package view.interaction.interactionelements;

import com.mxgraph.swing.mxGraphComponent;
import controller.editor.ProvenanceEditorController;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;

/**
 * Created by Marcus on 16.11.2017.
 */
public class ElementDropTargetListener extends DropTargetAdapter {
    ProvenanceEditorController controller;
    private mxGraphComponent targetPanel;
    private DropTarget dropTarget;

    public ElementDropTargetListener(mxGraphComponent targetPanel, ProvenanceEditorController controller) {
        this.controller = controller;
        this.targetPanel = targetPanel;
        this.dropTarget = new DropTarget(targetPanel, DnDConstants.ACTION_COPY_OR_MOVE, this, true, null);
    }

    @Override
    public void drop(DropTargetDropEvent event) {
        Component component = dropTarget.getComponent();
        Point dropPoint = component.getMousePosition();
        if (event.isDataFlavorSupported(DataFlavor.imageFlavor)) {
            controller.drawProvenanceElement(targetPanel, dropPoint.getX(), dropPoint.getY());
        }

    }
}
