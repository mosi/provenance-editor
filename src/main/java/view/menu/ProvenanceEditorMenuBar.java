package view.menu;


import controller.editor.ProvenanceEditorController;
import controller.editor.subcontroller.FileManagementController;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.io.IOException;


/**
 * Created by Marcus on 29.10.2017.
 */
public class ProvenanceEditorMenuBar extends JMenuBar {

    private JMenu projectMenu;

    private JMenuItem newModel;
    private JMenuItem saveModel;
    //  private JMenu loadMenu;
    private JMenuItem loadMenu;
    private JMenuItem saveXMLMenu;
    private JMenuItem layoutMenuItem;

    public ProvenanceEditorMenuBar(ProvenanceEditorController controller) {

        projectMenu = new JMenu("Project");
        newModel = new JMenuItem("New Model");
        saveModel = new JMenuItem("Save Model");
        loadMenu = new JMenuItem("Load Model");
        saveXMLMenu = new JMenuItem("Save XML");
        layoutMenuItem = new JMenuItem("Layout graph");

        loadMenu.addActionListener(e -> {
            FileManagementController fileManagementController = controller.getFileManagementController();
            fileManagementController.loadGraphFromFile();
        });
        saveModel.addActionListener(e -> {
            try {
                controller.getFileManagementController().saveGraphToFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        });
        newModel.addActionListener(e -> controller.createNewProvenanceModel());
        saveXMLMenu.addActionListener(e -> controller.getXmlExportController()
                .createProvenanceXMLFile(controller.getProvenanceModel()));


        layoutMenuItem.addActionListener(e-> controller.layoutGraph());

        setup();
    }


    private void setup() {
        projectMenu.add(newModel);
        projectMenu.add(saveModel);
        projectMenu.addSeparator();
        projectMenu.add(loadMenu);
        projectMenu.add(saveXMLMenu);
        projectMenu.add(layoutMenuItem);
        this.add(projectMenu);

    }

}
